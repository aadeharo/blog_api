//   Archivo de rutas

import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './components/users/users.component'
import { HomeComponent } from './components/home/home.component'
import { PostComponent } from './components/post/post.component'
import { ListadopostsComponent } from './components/listadoposts/listadoposts.component'


const routes: Routes = [

    { path: 'users', component: UsersComponent },
    { path: 'home', component: HomeComponent },
    { path: 'posts', component: ListadopostsComponent },            //Listado de post
    { path: 'post/:id', component: PostComponent },             //Post en concreto
    { path: '**', pathMatch:'full', redirectTo: 'home' }

    /* { path: 'home', component: HomeComponent },
    { path: 'heroes', component: HeroesComponent },
    { path: 'about', component: AboutComponent },
    { path: 'buscarheroes/:termino', component: BuscarheroesComponent },
    { path: 'crearheroe', component: CrearheroeComponent },
    { path: 'heroes/:id', component: HeroeComponent },              // id base de datos
    { path: 'heroe/:id', component: HeroeComponent },    //recibe parametro
    { path: '**', pathMatch:'full', redirectTo: 'home' } */  // ** quiere decir cualquier cosa que ingrese por url redirecta a home
    //  Nunca agregar path por debajo del path  **
];

export const appRouting = RouterModule.forRoot(routes);