import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { BlogsService } from '../servicios/blogs.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  userid : number
  users : any;

  constructor(private blogService : BlogsService, private router:Router) { }
  ngOnInit() 
  {
    this.blogService.getUsersAPI()
      .subscribe(data => {    //ejecuta la API
      console.log(data)
      this.users = data.results;     //    es el nombre del array de heroes (fijarse en postman)
      this.userid = data.id;
      this.blogService.saveUserID(this.userid)  //Guarda el ID del user
      },
      error => {
        console.log("fallo el call de la API");
        console.log(error)
     });

    console.log(this.users);

  }   // del ngOnInit

  setUserID(id:number){
    console.log('user id set '+id);
    this.blogService.saveUserID(id) //seteo id user
    this.router.navigate( [ '/posts'] )
  }

}
