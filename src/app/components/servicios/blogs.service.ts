import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class BlogsService {
    
    private users = [];     //  Todos los users
    private user: any;      //  Un usuario loggueado
    private posts = [];     //  Todos los posts del blog
    private post : any
    private userid : number
    private postid : number
    private comments = []
    private idP : number

    constructor(private http:Http) { 

        console.log("Servicio listo");
    }

    getUsersAPI()
    {
      console.log("llama a getUsersAPI");
    
       let header = new Headers({'Content-Type':'application/json'}); //header del postman
       let usersURL = "http://192.168.101.61:8080/social/users";    // IP de pc con la api
    
        return this.http.get(usersURL, {headers: header})
           .map(res =>{
             console.log(res.json());
             console.log("entro positivo REST getUsersAPI")
             this.users = res.json().results;     //   guarda en el array de heroes y no el mensaje de error
             return res.json();
           }, err => console.log("error: "+err.json()));
    }

    getPostsAPI()
    {
       console.log("llama a getPostsAPI");
    
       let header = new Headers({'Content-Type':'application/json'}); //header del postman
       let postsURL = "http://192.168.101.61:8080/social/posts";    // IP de pc con la api
    
        return this.http.get(postsURL, {headers: header})
           .map(res =>{
             console.log(res.json());
             console.log("entro positivo REST getPostsAPI")
             this.posts = res.json().results;     //   guarda en el array de heroes y no el mensaje de error
             return res.json();
        }, err => console.log("error: "+err.json()));
    }


    getPostAPI(id:number)      // Trae un post de un user y todos los comments
    {
       console.log("llama a getPostAPI");

       let header = new Headers({'Content-Type':'application/json'}); //header del postman
       let postsURL = "http://192.168.101.61:8080/social/comment/"+id   // IP de pc con la api
    
        return this.http.get(postsURL, {headers: header})
           .map(res =>{
             console.log(res.json());
             console.log("entro positivo REST getPostsAPI")
             this.post = res.json().post;     //   guarda el contenido del post
             this.comments = res.json().results  // guarda comentarios del post
             return res.json();
        }, err => console.log("error: "+err.json()));
    }

    comentarPost(comentario:any){
        console.log("llama a comentarPostAPI");
 
        let header = new Headers({'Content-Type':'application/json'}); //header del postman
        let postsURL = "http://192.168.101.61:8080/social/comment/"  // IP de pc con la api
        let body = comentario;
    
        return this.http.post(postsURL, body, {headers: header})
           .map(res =>{
             console.log(res.json());
             console.log("entro positivo REST postHeroesAPI")
             //comentario = res.json().results;    
             return res.json();
           }, err => console.log("error: "+err.json()));

    }

    saveUserID(id:number) {
        this.userid = id;
        console.log('user guardado:',this.userid);
    }

    getUserID(){
        return this.userid
    }

    savePostID(id:number) {
        this.postid = id;
        console.log('post guardado:',this.postid);
    }

    getPostID(){
        return this.postid
    }









}