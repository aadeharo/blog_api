import { Component, OnInit } from '@angular/core';
import { BlogsService } from '../servicios/blogs.service'
import { Router } from '@angular/router'


@Component({
  selector: 'app-listadoposts',
  templateUrl: './listadoposts.component.html',
  styleUrls: ['./listadoposts.component.css']
})
export class ListadopostsComponent implements OnInit {

  postid :number
  posts = []

  constructor(private blogsService:BlogsService, private router:Router) { }

  ngOnInit() 
  {
    
      this.blogsService.getPostsAPI()
        .subscribe(data => {    //ejecuta la API
        console.log(data)
        this.posts = data.results;     //    es el nombre del array de posts (fijarse en postman)
        this.postid = data.id;
        },
        error => {
          console.log("fallo el call de la API");
          console.log(error)
       });
  
      console.log(this.posts);
  }

  entrarAlPost(id:number){
    console.log('imprimo parametro de .ts '+id);
    this.blogsService.savePostID(id)
    this.router.navigate( [ '/post',id] )
  }



}
