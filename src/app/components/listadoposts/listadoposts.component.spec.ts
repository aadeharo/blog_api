import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadopostsComponent } from './listadoposts.component';

describe('ListadopostsComponent', () => {
  let component: ListadopostsComponent;
  let fixture: ComponentFixture<ListadopostsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadopostsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadopostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
