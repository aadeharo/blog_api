import { Component, OnInit } from '@angular/core';
import { BlogsService } from '../servicios/blogs.service'
import { ActivatedRoute } from "@angular/router";
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {


  comentario = {
    idUser: null,
    idPost: null,
    comment: ''
  }

  cadena: any
  userid:number
  mostrar : boolean = false

  constructor(private blogsService:BlogsService,private activatedRouted: ActivatedRoute) { 

    this.activatedRouted.params.subscribe(params => {
      console.log("Parametro recibidio es: "+params['id'])
      
      this.blogsService.getPostAPI(params['id']).subscribe(data => {
        console.log(data)
        this.cadena = data
       this.mostrar = true
      },
         error => {
           console.log("fallo el call de la API");
         
           console.log(error)
         });
      
    })
  }

  commentPost(forma:NgForm){
    console.log('Llamada a commentPost');
    
    this.comentario.idPost = this.blogsService.getPostID()
    this.comentario.idUser = this.blogsService.getUserID()

    this.blogsService.comentarPost(this.comentario)
      .subscribe(data => {    //ejecuta la API
      console.log(data)
      },
      error => {
        console.log("fallo el call de la API, desde commentPost post.component.ts");
        console.log(error)
     });
    console.log("comentario ", this.comentario);


    //Bloque para que refresque la pag automaticamente
    this.activatedRouted.params.subscribe(params => {
      console.log("Parametro recibidio es: "+params['id'])
      
      this.blogsService.getPostAPI(params['id']).subscribe(data => {
        console.log(data)
        this.cadena = data
       
      },
         error => {
           console.log("fallo el call de la API");
         
           console.log(error)
         });
      
    })






  }


  ngOnInit() {  }



}