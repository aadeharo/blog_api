import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';
import { FormsModule} from '@angular/forms';

import { NavbarComponent } from './components/navbar/navbar.component';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';

import { appRouting } from './app.routes';

import { BlogsService } from './components/servicios/blogs.service';
import { UsersComponent } from './components/users/users.component';
import { PostComponent } from './components/post/post.component';
import { ListadopostsComponent } from './components/listadoposts/listadoposts.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UsersComponent,
    HomeComponent,
    PostComponent,
    ListadopostsComponent,
   
  ],
  imports: [
    BrowserModule,
    appRouting,
    FormsModule,
    HttpModule
  ],
  providers: [BlogsService],      //  Proveedor
  bootstrap: [AppComponent]
})
export class AppModule { }
